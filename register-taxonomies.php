<?php

namespace cpl\cpl_src_book_recommendations;
/**
 * Registers the taxonomies
 *
 * @since 1.0.0
 *
 * @return void
 */
function register_taxonomy_reader_range() {
	$args = array(
		'labels'             => array(
			'menu_name'                  => __( 'Reader Ranges', 'cpl_src_book_recommendations' ),
			'all_items'                  => __( 'All Reader Ranges', 'cpl_src_book_recommendations' ),
			'edit_item'                  => __( 'Edit Reader Range', 'cpl_src_book_recommendations' ),
			'view_item'                  => __( 'View Reader Range', 'cpl_src_book_recommendations' ),
			'update_item'                => __( 'Update Reader Range', 'cpl_src_book_recommendations' ),
			'add_new_item'               => __( 'Add new Reader Range', 'cpl_src_book_recommendations' ),
			'new_item_name'              => __( 'New Reader Range Name', 'cpl_src_book_recommendations' ),
			'parent_item'                => __( 'Parent Reader Range', 'cpl_src_book_recommendations' ),
			'parent_item_colon'          => __( 'Parent Reader Range:', 'cpl_src_book_recommendations' ),
			'search_items'               => __( 'Search Reader Ranges', 'cpl_src_book_recommendations' ),
			'popular_items'              => __( 'Popular Reader Ranges', 'cpl_src_book_recommendations' ),
			'separate_items_with_commas' => __( 'Separate Reader Ranges with commas', 'cpl_src_book_recommendations' ),
			'add_or_remove_items'        => __( 'Add or remove Reader Ranges', 'cpl_src_book_recommendations' ),
			'choose_from_most_used'      => __( 'Choose most used Reader Ranges', 'cpl_src_book_recommendations' ),
			'not_found'                  => __( 'No Reader Ranges found', 'cpl_src_book_recommendations' ),
			'name'                       => _x( 'Reader Ranges', 'taxonomy general name' ),
			'singular_name'              => _x( 'Reader Range', 'taxonomy general name' ),
		),

		'public'             => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'show_in_nav_menus'  => true,
		'show_tagcloud'      => false,
		'show_in_quick_edit' => true,
		'show_admin_column'  => true,
		'show_in_rest'       => true,
		'hierarchical'       => false,
		'query_var'          => true,
		'rewrite'            => true,
	);
	register_taxonomy( 'reader_range', array( 'cpl_src_book_rec' ), $args );
}
add_action( 'init', __NAMESPACE__ . '\register_taxonomy_reader_range' );

function register_taxonomy_slc_book_theme() {
	$args = array(
		'labels'             => array(
			'menu_name'                  => __( 'SLC Book Themes', 'cpl_src_book_recommendations' ),
			'all_items'                  => __( 'All SLC Book Themes', 'cpl_src_book_recommendations' ),
			'edit_item'                  => __( 'Edit SLC Book Theme', 'cpl_src_book_recommendations' ),
			'view_item'                  => __( 'View SLC Book Theme', 'cpl_src_book_recommendations' ),
			'update_item'                => __( 'Update SLC Book Theme', 'cpl_src_book_recommendations' ),
			'add_new_item'               => __( 'Add new SLC Book Theme', 'cpl_src_book_recommendations' ),
			'new_item_name'              => __( 'New SLC Book Theme Name', 'cpl_src_book_recommendations' ),
			'parent_item'                => __( 'Parent SLC Book Theme', 'cpl_src_book_recommendations' ),
			'parent_item_colon'          => __( 'Parent SLC Book Theme:', 'cpl_src_book_recommendations' ),
			'search_items'               => __( 'Search SLC Book Themes', 'cpl_src_book_recommendations' ),
			'popular_items'              => __( 'Popular SLC Book Themes', 'cpl_src_book_recommendations' ),
			'separate_items_with_commas' => __( 'Separate SLC Book Themes with commas', 'cpl_src_book_recommendations' ),
			'add_or_remove_items'        => __( 'Add or remove SLC Book Themes', 'cpl_src_book_recommendations' ),
			'choose_from_most_used'      => __( 'Choose most used SLC Book Themes', 'cpl_src_book_recommendations' ),
			'not_found'                  => __( 'No SLC Book Themes found', 'cpl_src_book_recommendations' ),
			'name'                       => _x( 'SLC Book Themes', 'taxonomy general name' ),
			'singular_name'              => _x( 'SLC Book Theme', 'taxonomy general name' ),
		),

		'public'             => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'show_in_nav_menus'  => true,
		'show_tagcloud'      => false,
		'show_in_quick_edit' => true,
		'show_admin_column'  => true,
		'show_in_rest'       => true,
		'hierarchical'       => false,
		'query_var'          => true,
		'rewrite'            => true,
	);
	register_taxonomy( 'slc_book_theme', array( 'cpl_src_book_rec' ), $args );
}
add_action( 'init', __NAMESPACE__ . '\register_taxonomy_slc_book_theme' );
