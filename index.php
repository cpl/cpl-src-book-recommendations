<?php

namespace cpl\cpl_src_book_recommendations;

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://skorasaur.us
 * @since             1.0.1
 * @package           cpl_src_book_recommendations
 *
 * @wordpress-plugin
 * Plugin Name:       CPL SRC Book Recommendations
 * Plugin URI:        https://gitlab.com/cpl/cpl-src-book-recommendations
 * Description:
 *
 * Requires PHP:      7.2+
 * Version:           0.0.1
 * Author:            Will Skora
 * Author URI:        https://skorasaur.us
 * Update URI:        https://gitlab.com/cpl/cpl-src-book-recommendations
 * License:           GPL-3.0+
 * License URI:       https://choosealicense.com/licenses/gpl-3.0/
 * Text Domain:       cpl_src_book_recommendations
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// not sure if this a standard?
define( 'CPL_SBR_VERSION', '0.0.1' );
define( 'CPL_SBR_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );


require_once( CPL_SBR_PLUGIN_DIR . '/register-post-type.php' );
require_once( CPL_SBR_PLUGIN_DIR . '/register-taxonomies.php' );
// require_once( CPL_SBR_PLUGIN_DIR . '/register-post-meta.php' );

add_action( 'init', __NAMESPACE__ . '\register_cpl_src_book_rec', 0 );

// https://codex.wordpress.org/Function_Reference/register_post_type#Flushing_Rewrite_on_Activation
// This is to flush rewrites so if I decide to change the path of the CPT, it will auotmatically
// do it on plugin activation
function my_rewrite_flush() {
	// First, we "add" the custom post type via the above written function.
	// Note: "add" is written with quotes, as CPTs don't get added to the DB,
	// They are only referenced in the post_type column with a post entry,
	// when you add a post of this CPT.
	register_cpl_src_book_rec();

	// ATTENTION: This is *only* done during plugin activation hook in this example!
	// You should *NEVER EVER* do this on every page load!!
	\flush_rewrite_rules();
}
register_activation_hook( __FILE__, __NAMESPACE__ . '\my_rewrite_flush' );


// When you make changes to a ACF field group in the ACF GUI, the JSON file in /acf-json automatically
// updates to reflect the changes
function my_acf_json_save_point( $path ) {
	return CPL_SBR_PLUGIN_DIR . '/acf-json';
}
add_filter( 'acf/settings/save_json', __NAMESPACE__ . '\my_acf_json_save_point' );



// does not automatically load the JSON yet into the database; just creates a prompt in wp-admin/edit.php?post_type=acf-field-group
// that a sync is available
function my_acf_json_load_point( $paths ) {
	// Remove the original path (optional).
	unset( $paths[0] );

	// Append the new path and return it.
	$paths[] = CPL_SBR_PLUGIN_DIR . '/acf-json';

	return $paths;
}
add_filter( 'acf/settings/load_json', __NAMESPACE__ . '\my_acf_json_load_point' );
