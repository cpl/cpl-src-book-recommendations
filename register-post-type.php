<?php

namespace cpl\cpl_src_book_recommendations;
/**
 * Registers a custom post type 'cpl_citation'.
 *
 * @since 1.0.0
 *
 * @return void
 */
function register_cpl_src_book_rec() : void {
	$labels = array(
		'name'                  => _x( 'Book Recommendations', 'Post Type General Name', 'cpl_src_book_recommendations' ),
		'singular_name'         => _x( 'Book Recommendation', 'Post Type Singular Name', 'cpl_src_book_recommendations' ),
		'menu_name'             => __( 'Book Recommendations', 'cpl_src_book_recommendations' ),
		'name_admin_bar'        => __( 'Book Recommendations', 'cpl_src_book_recommendations' ),
		'archives'              => __( 'Book Recommendations Archives', 'cpl_src_book_recommendations' ),
		'attributes'            => __( 'Book Recommendations Attributes', 'cpl_src_book_recommendations' ),
		'parent_item_colon'     => __( 'Parent Book Recommendation:', 'cpl_src_book_recommendations' ),
		'all_items'             => __( 'All Book Recommendations', 'cpl_src_book_recommendations' ),
		'add_new_item'          => __( 'Add New Book Recommendation', 'cpl_src_book_recommendations' ),
		'add_new'               => __( 'Add New', 'cpl_src_book_recommendations' ),
		'new_item'              => __( 'New Book Recommendation', 'cpl_src_book_recommendations' ),
		'edit_item'             => __( 'Edit Book Recommendation', 'cpl_src_book_recommendations' ),
		'update_item'           => __( 'Update Book Recommendation', 'cpl_src_book_recommendations' ),
		'view_item'             => __( 'View Book Recommendation', 'cpl_src_book_recommendations' ),
		'view_items'            => __( 'View Book Recommendations', 'cpl_src_book_recommendations' ),
		'search_items'          => __( 'Search Book Recommendations', 'cpl_src_book_recommendations' ),
		'not_found'             => __( 'Book Recommendation Not Found', 'cpl_src_book_recommendations' ),
		'not_found_in_trash'    => __( 'Book Recommendation Not Found in Trash', 'cpl_src_book_recommendations' ),
		'featured_image'        => __( 'Featured Image', 'cpl_src_book_recommendations' ),
		'set_featured_image'    => __( 'Set Featured Image', 'cpl_src_book_recommendations' ),
		'remove_featured_image' => __( 'Remove Featured Image', 'cpl_src_book_recommendations' ),
		'use_featured_image'    => __( 'Use as Featured Image', 'cpl_src_book_recommendations' ),
		'insert_into_item'      => __( 'Insert into Book Recommendation', 'cpl_src_book_recommendations' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Book Recommendation', 'cpl_src_book_recommendations' ),
		'items_list'            => __( 'Book Recommendations List', 'cpl_src_book_recommendations' ),
		'items_list_navigation' => __( 'Book Recommendations List Navigation', 'cpl_src_book_recommendations' ),
		'filter_items_list'     => __( 'Filter Book Recommendations List', 'cpl_src_book_recommendations' ),
	);
	$labels = apply_filters( 'cpl_src_book_rec-labels', $labels );
	$args   = array(
		'label'               => __( 'Book Recommendation', 'cpl_src_book_recommendations' ),
		'description'         => __( 'Book Recommendation ', 'cpl_src_book_recommendations' ),
		'labels'              => $labels,
		'supports'            => array(
			'title',
			'editor',
			'author',
			'custom-fields',
			'page-attributes',
			'thumbnail',
		),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-book',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'can_export'          => true,
		'map_meta_cap'        => true,
		'show_in_rest'        => true,
	);
	$args   = apply_filters( 'cpl_src_book_rec-args', $args );

	register_post_type( 'cpl_src_book_rec', $args );
}


// // determine which columns at edit.php?post_type=citation display
// // seems a little hacky in that I just generated
// // t3h_agenda_date; you can also rename column_name as well
// function modify_cpl_citation_post_admin_columns( $columns ) {
// 	unset( $columns['author'] );
// 	unset( $columns['date'] );
// 	unset( $columns['last-modified'] );
// 	return array_merge(
// 		$columns,
// 	);
// }
// add_filter( 'manage_cpl_citation_posts_columns', __NAMESPACE__ . '\modify_cpl_citation_post_admin_columns' );
